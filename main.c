/*
 * Copyright (c) 2015 Sergi Granell (xerpi)
 */

#include <string.h>

#include <psp2/display.h>
#include <psp2/ctrl.h>
#include <psp2/apputil.h>
#include <psp2/sysmodule.h>
#include <psp2/kernel/processmgr.h>

#include <taihen.h>
#include <ftpvita.h>

#include "utils.h"
#include "console.h"

#define MOD_PATH "ux0:data/tai/kplugin.skprx"

#define LOADER_PORT 1338

#define EXIT_MASK (SCE_CTRL_START)
#define LOADER_MASK (SCE_CTRL_CROSS | SCE_CTRL_SQUARE | SCE_CTRL_CIRCLE | SCE_CTRL_TRIANGLE)

static int loader_run;
static int loader_sockfd;

static int loader_thread(SceSize args, void *argp)
{
	int ret;
	UNUSED(ret);

	SceNetSockaddrIn loaderaddr;

	DEBUG("Loader thread started!\n");

	/* Create server socket */
	loader_sockfd = sceNetSocket("ftploader_loader_sock",
		SCE_NET_AF_INET,
		SCE_NET_SOCK_STREAM,
		0);

	DEBUG("Loader socket fd: %d\n", loader_sockfd);

	/* Fill the server's address */
	loaderaddr.sin_family = SCE_NET_AF_INET;
	loaderaddr.sin_addr.s_addr = sceNetHtonl(SCE_NET_INADDR_ANY);
	loaderaddr.sin_port = sceNetHtons(LOADER_PORT);

	/* Bind the server's address to the socket */
	ret = sceNetBind(loader_sockfd, (SceNetSockaddr *)&loaderaddr, sizeof(loaderaddr));
	DEBUG("Loader: sceNetBind(): 0x%08X\n", ret);

	/* Start listening */
	ret = sceNetListen(loader_sockfd, 128);
	DEBUG("Loader: sceNetListen(): 0x%08X\n", ret);

	while (loader_run) {
		/* Accept clients */
		SceNetSockaddrIn clientaddr;
		int client_sockfd;
		unsigned int addrlen = sizeof(clientaddr);

		DEBUG("Loader: Waiting for incoming loader connections...\n");

		client_sockfd = sceNetAccept(loader_sockfd, (SceNetSockaddr *)&clientaddr, &addrlen);
		if (client_sockfd >= 0) {
			DEBUG("Loader: New connection, client fd: 0x%08X\n", client_sockfd);
			sceNetSocketClose(client_sockfd);

			SceUID mod_id;
			tai_module_args_t arg1;
			arg1.size = sizeof(arg1);
			arg1.pid = KERNEL_PID;
			arg1.args = 0;
			arg1.argp = NULL;
			arg1.flags = 0;
			mod_id = taiLoadStartKernelModuleForUser(MOD_PATH, &arg1);
			if (mod_id < 0) {
				INFO("Error loading the skprx: 0x%08X\n", mod_id);
				continue;
			}

			INFO("Plugin loaded! Press X, O, [] or /\\ to stop it.\n");

			while (loader_run) {
				SceCtrlData pad;
				sceCtrlReadBufferPositive(0, &pad, 1);
				if (pad.buttons & LOADER_MASK)
					break;
				sceKernelDelayThread(50 * 1000);
			}

			tai_module_args_t arg2;
			arg2.size = sizeof(arg2);
			arg2.pid = KERNEL_PID;
			arg2.args = 0;
			arg2.argp = NULL;
			arg2.flags = 0;
			ret = taiStopUnloadKernelModuleForUser(mod_id, &arg2, NULL, NULL);
			DEBUG("Stop unload module: 0x%08X\n", ret);

			INFO("Plugin stopped!\n");

			video_set_fb();
		} else {
			break;
		}
	}

	DEBUG("Loader thread exiting!\n");

	sceKernelExitDeleteThread(0);
	return 0;
}

int main()
{
	int run = 1;
	char vita_ip[16];
	unsigned short int vita_port;
	SceCtrlData pad;
	SceUID loader_thid;

	sceKernelPowerTick(SCE_KERNEL_POWER_TICK_DISABLE_AUTO_SUSPEND);
	sceKernelPowerTick(SCE_KERNEL_POWER_TICK_DISABLE_OLED_OFF);

	init_video();
	console_init();
	console_set_color(WHITE);

	sceSysmoduleLoadModule(SCE_SYSMODULE_NET);

	INFO("ftploader by xerpi\n");
	INFO("Press START to exit\n");

	int y = console_get_y();
	while (ftpvita_init(vita_ip, &vita_port) < 0 && run) {
		INFO("You have to enable Wi-Fi.\nPress X to continue.\n");
		while (run) {
			sceCtrlReadBufferPositive(0, &pad, 1);
			if (pad.buttons & SCE_CTRL_CROSS) break;
			else if (pad.buttons & SCE_CTRL_SQUARE) run = 0;
			sceDisplayWaitVblankStart();
		}
		if (!run) {
			INFO("Exiting...\n");
			console_fini();
			end_video();
			return 0;
		}
		console_set_y(y);
	}

	/* Create loader thread */
	loader_thid = sceKernelCreateThread("ftploader_loader_thread",
		loader_thread, 0x10000100, 0x10000, 0, 0, NULL);
	DEBUG("Loader thread UID: 0x%08X\n", loader_thid);

	loader_run = 1;
	sceKernelStartThread(loader_thid, 0, NULL);

	ftpvita_add_device("ux0:");
	ftpvita_add_device("ur0:");

	console_set_color(LIME);
	INFO("ftploader IP %s FTP Port %i, Loader port: %i\n", vita_ip, vita_port, LOADER_PORT);
	console_set_color(WHITE);

	console_set_top_margin(10 + 20*3);

	while (run) {
		sceCtrlReadBufferPositive(0, &pad, 1);
		if (pad.buttons & EXIT_MASK)
			run = 0;
		sceKernelDelayThread(100 * 1000);
	}

	INFO("Exiting...\n");

	loader_run = 0;
	sceNetSocketClose(loader_sockfd);
	sceKernelWaitThreadEnd(loader_thid, NULL, NULL);

	ftpvita_fini();

	console_fini();
	end_video();
	sceKernelExitProcess(0);
	return 0;
}
